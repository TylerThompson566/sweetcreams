Issues
	fix command loader only adding one command despite reading multiple from disk
		- get the files in a list
		- get the classes from the files
		- instantiate each class into an object
	fix logger so it prints to the log file as well
		- cant use appender rolling log or something, first log that appears
	test edge cases for command loading and handle error scenarios gracefully
		- returning null in runCommand
		- doesn't extend Command/implement Creamable
TODO
	check text compare in STS/Eclipse
	Command Ideas
		roll
		flip
		points system
			requires db