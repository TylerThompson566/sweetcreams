package runner;

import org.apache.logging.log4j.Logger;

import controller.StartupController;

import org.apache.logging.log4j.LogManager;

/**
 * This class is the class that contains the main method and starts the
 * execution of the bot
 * 
 * @author tyler
 *
 */
public class MainRunner {

	/** The logger */
	private final static Logger log = LogManager.getLogger(MainRunner.class);

	/** the Client controller */
	private static StartupController startupController;

	/**
	 * Main method, start of execution
	 * 
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {

		// declare source for debugging
		final String source = "main() ";

		// instantiate the bot
		log.info(source + "Intantiating StartUpController...");
		startupController = StartupController.getInstance();
		
		//if the startup controller is null, exit gracefully
		if (startupController == null) {
			log.error(source + "Could not instantiate the StartupController, exiting gracefully...");
			return;
		}
		
		//if we reach here, we initialized the startup controller, start the bot
		log.info(source + "Instantiating the bot complete, starting the bot...");
		startupController.startBot();
		
		
		
	}
}
