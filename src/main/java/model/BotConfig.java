package model;

/**
 * This class is a model for the bot configurations read from configuration.yml
 * 
 * @author tyler
 *
 */
public class BotConfig {

	/** The client ID */
	private String clientID;

	/** The client secret */
	private String clientSecret;

	/** The bot token */
	private String token;

	/** The name of the bot */
	private String username;

	/** The Bot ID (the 4 digits at the end of the username starting with #) */
	private int botID;

	/** The prefix string */
	private String prefix;

	/**
	 * The constructor for the BotConfig
	 */
	public BotConfig(String clientId, String clientSecret, String token, String username, int botID, String prefix) {

		// set the arguments to the parameters
		this.clientID = clientId;
		this.clientSecret = clientSecret;
		this.token = token;
		this.username = username;
		this.botID = botID;
		this.prefix = prefix;
	}

	/**
	 * Default constructor for BotConfig
	 */
	public BotConfig() {
		clientID = null;
		clientSecret = null;
		token = null;
		username = null;
		botID = 0;
		prefix = null;
	}

	// accessors
	public String getClientID() {
		return clientID;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public String getToken() {
		return token;
	}

	public String getUsername() {
		return username;
	}

	public int getBotID() {
		return botID;
	}

	public String getPrefix() {
		return prefix;
	}

	// mutators
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setBotID(int botID) {
		this.botID = botID;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
