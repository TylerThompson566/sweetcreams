package model;

/**
 * This interface, if implemented, makes that class a command that can be
 * executed by the bot
 * 
 * @author tyler
 *
 */
public interface Creamable {

	/**
	 * Run the command
	 * 
	 * @return the string that will be sent to the discord chat via the bot
	 */
	public String runCommand(String command);
}
