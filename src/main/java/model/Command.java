package model;

/**
 * This class defines what a command is. All commands will implement Creamable
 * which means it has to have the runCommand method. We will call that method
 * when we match the class to a command from the discord chat
 * 
 * @author tyler
 *
 */
public abstract class Command implements Creamable {

	/** the name of the command */
	private String command;

	/**
	 * Get the command name
	 * 
	 * @return
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * Set the command name
	 * 
	 * @param command the new command name
	 */
	public void setCommand(String command) {
		this.command = command;
	}
}