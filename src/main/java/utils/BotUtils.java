package utils;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RequestBuffer;

/**
 * This class holds the utility logic for the bot
 * 
 * @author tyler
 *
 */
public final class BotUtils {

	/** The logger */
	private final static Logger log = LogManager.getLogger(BotUtils.class);

	private BotUtils() {
	}

	/**
	 * Build the discord client
	 * 
	 * @param token the token used to log in
	 * @return the built IDiscordClient, or null if failure
	 */
	public static IDiscordClient buildDiscordClient(String token) {

		// declare source for logging
		final String source = "buildDiscordClient() ";

		// try to build the discord client and return it
		try {
			return new ClientBuilder().withToken(token).build();

			// if failed, return null
		} catch (Exception e) {
			log.error(source + "Error: Failed to build discord client");
			log.error(source, e);
			return null;
		}
	}

	/**
	 * Helper Function to send a message to the discord channel
	 * 
	 * @param channel the channel to send the message to
	 * @param message the message to send
	 */
	public static void sendMessage(IChannel channel, String message) {

		// declare source for logging
		final String source = "sendMessage() ";

		// add the request to send a message to the request buffer
		RequestBuffer.request(() -> {

			// try to send the message
			try {
				log.info(source + "Sending message \"" + message + "\" to channel " + channel.getName() + "...");
				channel.sendMessage(message);

				// catch the exception and log the error
			} catch (DiscordException e) {
				log.error(source + "Error: Could not add the request \"" + message + "\" to channel "
						+ channel.getName());
				log.error(source, e);
			}
		});

		// log success if we reach here
		log.info(source + "Successfully sent message \"" + message + "\" to channel " + channel.getName());
	}
	
	/**
	 * Get a decoded file path from a URL object
	 * @param resource the URL
	 * @return a String that represents the path to the file on disk
	 */
	public static String getFilePathStringFromURL(URL resource) {
		
		//define source for debugging
		String source = "getFilePathStringFromURL() ";
		
		//try to decode the URL from the file path
		try {
			return URLDecoder.decode(resource.getPath(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error(source + "Unsupported encoding - UTF-8. returning null...");
			return null;
		}
	}
}
