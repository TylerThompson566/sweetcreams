package controller;

import java.io.File;
import java.net.URL;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.gson.Gson;

import model.BotConfig;
import runner.MainRunner;
import utils.BotUtils;

/**
 * This class manages loading in configuration files from disk. This class
 * follows the singleton design pattern so there can only be one instance of
 * this class
 * 
 * @author tyler
 *
 */
public class ConfigurationManager {

	/** The logger */
	private static final Logger log = LogManager.getLogger(MainRunner.class);

	/** The one instance of the configuration manager */
	private static ConfigurationManager manager;

	/** the object that stores the configuration of the bot */
	private static BotConfig config;

	/** the object that is used to serialize pojos to JSON objects */
	private Gson gson;

	/**
	 * Private constructor for the configuration manager
	 */
	private ConfigurationManager() {

		// initialize objects
		gson = new Gson();

		// read the configuation.yml file into config
		loadConfig();
	}

	/**
	 * Get an instance of the ConfigurationManager
	 * 
	 * @return the ConfigurationManager
	 */
	public static synchronized ConfigurationManager getInstance() {

		// if the manager is null, create a new configuration manager
		if (manager == null) {
			manager = new ConfigurationManager();
		}
		
		// at this point, the manager has to be instantiated. if the config is null, reset the manager and return null
		if (config == null) {
			manager = null;
			return null;
		}

		// return the instance of the config manager
		return manager;
	}

	/**
	 * Load the bot config from the configuration.yml file and map it to the
	 * BotConfig pojo
	 */
	public void loadConfig() {

		// declare source for logging
		final String source = "loadConfig() ";

		// create a mapper object
		log.info(source + "Loading config...");
		log.info(source + "Mapping configuration.yml to config object...");
		ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

		// try to map the object to the yaml file
		try {
			
			//get the class loader and assert that it is not null
			ClassLoader classLoader = getClass().getClassLoader();
			assert classLoader != null;
			
			//get the url representing the file location
			URL configResource = classLoader.getResource("configuration.yml");
			log.info(source + "configuration.yml resource = " + configResource.getPath());
			
			//map the file to the bot config pojo
			config = mapper.readValue(new File(BotUtils.getFilePathStringFromURL(configResource)),
					BotConfig.class);

		// if we failed to read the config file, then log the error and set the config
		// to null before returning false
		} catch (Exception e) {
			log.error(source + "Error: Failed to read config file, printing stack trace...");
			log.error(source, e);
			config = null;
			return;
		}

		// log success
		log.info(source + "successfully read configuration.yml and mapped to BotConfig pojo");
		log.info(source + "BotConfig JSON: " + getConfigJSON());
	}

	/**
	 * Get the BotConfig object in a JSON format
	 * 
	 * @param gson the Gson object used to serialize the object
	 * @return a String representing the BotConfig object in JSON format
	 */
	public String getConfigJSON() {
		return gson.toJson(config);
	}

	/**
	 * determine if the configurations were loaded properly
	 * 
	 * @return true if the configurations were read from the file and mapped to the
	 *         pojo properly
	 */
	public boolean isLoaded() {
		return (config != null);
	}

	/**
	 * Get the bot configuration
	 * 
	 * @return the BotConfig object
	 */
	public BotConfig getBotConfig() {
		return config;
	}

	/**
	 * Get the Gson object
	 * 
	 * @return the Gson object
	 */
	public Gson getGson() {
		return gson;
	}
}
