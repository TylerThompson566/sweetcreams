package controller;

import org.apache.logging.log4j.Logger;

import sx.blah.discord.api.IDiscordClient;
import utils.BotUtils;

import org.apache.logging.log4j.LogManager;

/**
 * This class manages the initialization and startup of the bot. This class
 * follows the singleton design pattern so only one instance of this class can
 * exist
 * 
 * @author tyler
 *
 */
public final class StartupController {

	/** The logger */
	private static final Logger log = LogManager.getLogger(StartupController.class);

	/** The single instance of the client controller */
	private static StartupController controller;

	/** The Configuration Manager */
	private static ConfigurationManager config;

	/** The discord client */
	private static IDiscordClient discordClient;

	/** The Bot command controller */
	private static BotCommandListener botCommandListener;

	/**
	 * Constructor for the controller
	 */
	private StartupController() {

		// get instances or instantiate any needed objects
		config = ConfigurationManager.getInstance();
		botCommandListener = BotCommandListener.getInstance(config.getBotConfig().getPrefix());
	}

	/**
	 * Get an instance of the ClientController
	 * 
	 * @return the controller
	 */
	public static synchronized StartupController getInstance() {

		// initialize the controller if needed
		if (controller == null) {
			controller = new StartupController();
		}

		// if we failed to load either the config or the bot command listener is null,
		// reset the controller and return null
		if (config == null || botCommandListener == null) {
			controller = null;
			return null;
		}

		// return an instance of the controller
		return controller;
	}

	/**
	 * Start the bot
	 */
	public void startBot() {

		// declare the source for logging
		final String source = "startBot() ";

		// if the discord client is null, then initialize it
		log.info(source + "initializing bot...");
		if (discordClient == null) {

			// run the initialization function and check the result
			// if the initialization failed, return to exit gracefully
			if (!initBot()) {
				return;
			}
		}

		// log the bot in
		log.info(source + "logging the bot in...");
		discordClient.login();
	}

	/**
	 * Start the bot
	 * 
	 * @return true if the bot is initialized
	 */
	private boolean initBot() {

		// declare source for logging
		final String source = "initBot() ";

		// try to load the configurations. if we fail, return false
		log.info(source + "checking Config...");
		if (!config.isLoaded()) {
			log.error(source + "Failed to load config upon initialization");
			return false;
		}
		log.info("successfully checked and loaded config.");

		// build the discord client
		log.info(source + "Building discord client...");
		discordClient = BotUtils.buildDiscordClient(config.getBotConfig().getToken());

		// if the discord client is null, then build failed. return to exit gracefully
		if (discordClient == null) {
			log.error(source + "Failed to build discord client");
			return false;
		}

		// register the listener
		log.info(source + "Registering Listener...");
		discordClient.getDispatcher().registerListener(botCommandListener);
		log.info(source + "registered listener");

		// everything is configured, return true
		return true;
	}

}
