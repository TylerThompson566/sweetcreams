package controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import utils.BotUtils;

/**
 * This class handles all incoming commands and messages
 * 
 * @author tyler
 *
 */
public final class BotCommandListener {

	/** The logger */
	private final static Logger log = LogManager.getLogger(BotCommandListener.class);

	/** The one instance of the bot command controller */
	private static BotCommandListener listener;

	/** The command loader */
	private static CommandLoader commandLoader;

	/** The prefix string */
	private String prefix;

	/**
	 * The private constructor of the bot command controller
	 */
	private BotCommandListener(String prefix) {

		// set the prefix
		this.prefix = prefix;

		// create the command logger
		commandLoader = CommandLoader.getInstance();

		// create a list of commands
		commandLoader.logCommandClasses();

	}

	/**
	 * Get an instance of the bot command controller
	 * 
	 * @return an instance of a BotCommandController
	 */
	public static synchronized BotCommandListener getInstance(String prefix) {

		// if listener is null, then initialize it
		if (listener == null) {
			listener = new BotCommandListener(prefix);
		}

		// if the command loader is null, reset the listener and return null
		if (commandLoader == null) {
			listener = null;
			return null;
		}

		// return the instance of the listener
		return listener;
	}

	/**
	 * What to do when a message is received
	 * 
	 * @param event the event that was triggered
	 */
	@EventSubscriber
	public void onMessageReceived(MessageReceivedEvent event) {

		// declare source for debugging
		final String source = ("onMessageReceived() ");

		// read message and check to see if it is a command.
		// if so, run the matching command
		log.info(source + "Received event: channel=" + event.getChannel() + " msg=" + event.getMessage());

		// if the message is the help command, specially run the help command
		if (event.getMessage().getContent().equalsIgnoreCase(prefix + "help")) {
			log.error(source + "running help command...");
			BotUtils.sendMessage(event.getChannel(), commandLoader.getHelpMessage(prefix));
		}

		// if the message is some other command, try to run it
		else if (event.getMessage().getContent().startsWith(prefix)) {

			// try to run the command
			log.info(source + "event is command, running relevant command...");
			String message = CommandLoader.runCommand(event.getMessage().getContent(), prefix);

			// send the message to the discord channel
			BotUtils.sendMessage(event.getChannel(), message);
		}
	}
}