package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.Command;
import utils.BotUtils;

/**
 * This class finds all of the commands from the commands package and manages
 * them
 * 
 * @author tyler
 *
 */
public class CommandLoader {

	/** The logger */
	private static final Logger log = LogManager.getLogger(CommandLoader.class);

	/** the one instance of the command loader */
	private static CommandLoader loader;

	/** the commands package name */
	private static final String COMMANDS_PACKAGE = "commands";

	private static ArrayList<Command> commands;

	/**
	 * Private constructor for the command loader
	 */
	private CommandLoader() {

		// get commands from the commands package
		commands = getCommands();
	}

	/**
	 * Get one instance of the command loader
	 * 
	 * @return
	 */
	public static synchronized CommandLoader getInstance() {

		// if the loader is null, initialize it
		if (loader == null) {
			loader = new CommandLoader();
		}

		// if the commands list is null, reset the loader and return null
		if (commands == null) {
			loader = null;
			return null;
		}

		// return the instance of the loader
		return loader;
	}

	/**
	 * Run a command and give back the string to put in the chat
	 * 
	 * @param content the command entered
	 * @return the text to be sent as a message in the discord chat
	 */
	public static String runCommand(String content, String prefix) {

		// define source for logging
		final String source = "runCommand() ";

		// find the command with the matching name
		log.info(source + "searching for matching command for " + content);
		for (Command c : commands) {

			// if the command entered starts with the name of the command excluding the
			// prefix, then return the results of the command
			log.info(source + "checking command " + c.getCommand() + "...");
			if (content.toLowerCase().startsWith(c.getCommand(), prefix.length())) {
				log.info(source + "Found matching command, running command " + prefix + content);
				return c.runCommand(content);
			}
		}

		// if we found no matching command, then return error message
		log.info(source + "no matching command found, returning error message");
		return returnErrorMessage();
	}

	/**
	 * Send back an error message
	 * 
	 * @return the error message
	 */
	private static String returnErrorMessage() {
		return "Command not recognized";
	}

	/**
	 * Get all commands from the commands package
	 * 
	 * @return a list of command objects that can be run as commands, or null if an
	 *         error occurred
	 */
	@SuppressWarnings("rawtypes")
	public synchronized ArrayList<Command> getCommands() {

		// define source for debugging
		final String source = "getCommands() ";

		// get an array list of classes from the package
		try {

			// get a list of classes and create a list of commands
			log.info(source + "finding classes");
			ArrayList<Class> classes = getClasses(COMMANDS_PACKAGE);

			// if we found no classes, return null for error
			if (classes == null || classes.isEmpty()) {
				log.error(source + "found no classes in commands package");
				return null;
			}

			// create a new list of commands
			ArrayList<Command> commands = new ArrayList<Command>();

			// for each class in classes...
			for (Class c : classes) {

				// create an instance of the object, cast it to a Command object then add it to
				// the list
				log.info(source + "adding " + c.getName() + " to command list");
				Class<?> clazz = Class.forName(c.getName());
				Object command = clazz.newInstance();
				commands.add((Command) command);

				// return the list of commands
				logCommands();
				return commands;
			}

			// if we reach here, error occurred
		} catch (Exception e) {
			log.error(source + "Failed to read classes, printing stack trace...");
			log.error(source, e);
		}

		// return null if we reach here, error scenario
		return null;
	}

	/**
	 * Print out all command class names
	 */
	@SuppressWarnings("rawtypes")
	public synchronized void logCommandClasses() {

		// define source for debugging
		final String source = "logCommandClasses() ";

		// try to find all classes in the commands package
		try {
			log.info(source + "Printing out all classes in commands package...");
			ArrayList<Class> classes = getClasses(COMMANDS_PACKAGE);
			for (Class c : classes) {
				log.info(source + c.getName());
			}

			// log if something goes wrong
		} catch (Exception e) {
			log.error(source + "Failed to read classes, printing stack trace...");
			log.error(source, e);
		}
	}

	/**
	 * log all commands loaded to the command loaded
	 */
	public synchronized void logCommands() {

		// define source for debugging
		final String source = "logCommands() ";

		// try to find all classes in the commands package
		try {
			log.info(source + "Printing out all commands in commands list...");
			for (Command c : commands) {
				log.info(source + c.getCommand());
			}

			// log if something goes wrong
		} catch (Exception e) {
			log.error(source + "Failed to read classes, printing stack trace...");
			log.error(source, e);
		}
	}

	/**
	 * Scans all classes accessible from the context class loader which belong to
	 * the given package and sub-packages.
	 *
	 * @param packageName The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	private static ArrayList<Class> getClasses(String packageName) throws ClassNotFoundException, IOException {

		// define source for debugging
		final String source = "getClasses() ";

		// get the class loader and assert that it is not null
		log.debug(source + "Loading ClassLoader... ");
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		log.debug(source + "Loaded ClassLoader, classLoader = " + classLoader);
		assert classLoader != null;

		// replace the package name with / to make it a path on disk rather than a
		// package name
		String path = packageName.replace('.', '/');

		// create an enumeration of paths from the package
		Enumeration<URL> resources = classLoader.getResources(path);

		// create a list of files/directories
		ArrayList<File> dirs = new ArrayList<File>();

		// for each file in the enumeration, add it to the list
		log.debug(source + "grabbing URL from ClassLoader resources...");
		while (resources.hasMoreElements()) {
			URL resource = (URL) resources.nextElement();
			String fileString = BotUtils.getFilePathStringFromURL(resource);
			dirs.add(new File(fileString));
			log.debug(source + "added URL " + fileString + " to dirs");
		}

		// create an array of classes
		ArrayList<Class> classes = new ArrayList<Class>();

		// for each file, add its respective class to the list
		for (File directory : dirs) {
			log.debug(source + "adding all classes from " + directory.getPath() + " to classes");
			classes.addAll(findClasses(directory, packageName));
		}

		// return the list of classes
		return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * sub-directories.
	 *
	 * @param directory   The base directory
	 * @param packageName The package name for classes found inside the base
	 *                    directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	private static ArrayList<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {

		// define source for debugging
		final String source = "findClasses() ";

		// create a list of classes
		ArrayList<Class> classes = new ArrayList<Class>();

		// if the directory does not exist, return an empty list
		log.debug(source + directory.getPath() + " exists = " + directory.exists());
		if (!directory.exists()) {
			return classes;
		}

		// get an array of files from the directory
		File[] files = directory.listFiles();

		// check each file in the array
		log.debug(source + "iterating over all items in " + directory.getPath());
		for (File file : files) {

			// if the file is a directory, check the sub-directory for classes
			if (file.isDirectory()) {

				// the filename must not contain a .
				log.debug(source + "item " + file.getPath() + " is a directory, recursively calling findClasses()");
				assert !file.getName().contains(".");

				// add all of the classes found in the sub-directory and add it to the callers
				// array
				classes.addAll(findClasses(file, packageName + "." + file.getName()));

				// if the file is a class file, add it to the list of classes
			} else if (file.getName().endsWith(".class")) {
				log.debug(source + "item " + file.getName().substring(0, file.getName().length() - 6)
						+ " is a class file, add it to the list of classes");
				classes.add(
						Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}

		// return the list of classes
		return classes;
	}

	/**
	 * send the help message to the user
	 * 
	 * @return
	 */
	public String getHelpMessage(String prefix) {

		// create a list of commands and return it to be printed out
		StringBuilder sb = new StringBuilder("Commands:\n");
		for (Command c : commands) {
			sb.append(prefix + c.getCommand() + "\n");
		}
		return sb.toString();
	}
}
