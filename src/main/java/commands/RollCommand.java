package commands;

import model.Command;

/**
 * The roll command returns a random number from 1 to 100 inclusive
 * 
 * @author tyler
 *
 */
public class RollCommand extends Command {

	/**
	 * Constructor for the command
	 * 
	 * @param command the name of the command
	 */
	public RollCommand() {

		// set the command name to roll
		// THIS IS REQUIRED TO DO IN THE CONSTRUCTOR
		super.setCommand("roll");
	}

	@Override
	public String runCommand(String command) {
		int rand = (int) ((Math.random() * 100) + 1);
		return Integer.toString(rand);
	}
}