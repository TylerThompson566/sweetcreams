package commands;

import model.Command;

public class FlipCommand extends Command {

	public FlipCommand() {
		super.setCommand("flip");
	}

	@Override
	public String runCommand(String command) {

		// get a random number from 0-1.999999999 then round down so its either 0 or 1
		int flip = (int) (Math.random() * 2);

		if (flip == 0)
			return "You flipped Heads";
		else
			return "You flipped Tails";
	}

}
